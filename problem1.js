//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )
function findAllPeopleByJob(dataset , wantedJob) {
    if(Array.isArray(dataset) && typeof wantedJob == 'string') {
        const persons = dataset.filter((person) => {
            return person.job.toLowerCase().includes(wantedJob.toLowerCase());
        })
        if(persons.length == 0 ) {
            throw new Error(`No one found with ${wantedJob} job.`);
        }
        return persons;
    } else {
        throw new Error('Arguments passed to function findAllpeopleByJob  is/are not valid' );
    }
}
module.exports = findAllPeopleByJob;