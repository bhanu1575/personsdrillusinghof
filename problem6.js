// 6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).

const updateSalary = require('./problem3.js');


function getAvgSalariesOfAllCountries(dataset) {
    if(Array.isArray(dataset)) {
        updateSalary(dataset);

        let avgSalariesOfAllCountries = dataset.reduce((accumulator, person) => {
            if(accumulator.hasOwnProperty(person.location)) {
                let totalSalary = ( accumulator[person.location].avg_salary * accumulator[person.location].count )
                let newTotalSalary = totalSalary  + person.updated_salary ;
                let totalCount = accumulator[person.location].count + 1;
                accumulator[person.location].avg_salary = newTotalSalary/totalCount;
                accumulator[person.location].count = totalCount;

            } else {
                accumulator[person.location] = {'avg_salary':person.updated_salary, 'count': 1};
            }
            return accumulator;
        },{});
        let updatedAvgSalaries = Object.entries(avgSalariesOfAllCountries).reduce((accumulator,[country,info]) => {
            accumulator[country] = info.avg_salary;
            return accumulator;
        },{});
        return updatedAvgSalaries;

    } else {
        throw new Error('Arguments passed to the function getAvgSalaryOfAllCountries is not valid type');
    }
}

module.exports = getAvgSalariesOfAllCountries;