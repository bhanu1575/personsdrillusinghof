//    4. Find the sum of all salaries.

const updateSalary = require('./problem3.js')
function findSumOfAllSalaries (dataset) {
    if(Array.isArray(dataset)){
        updateSalary(dataset);
        let sumOfAllSalaries = dataset.reduce((accumulator, person) => {
            if(person.updated_salary) {
                return accumulator + person['updated_salary'];
            }
        },0);
        return sumOfAllSalaries
    } else {
        throw new Error('Arguments passed to the function sumOfAllSalaries is not valid.')
    }
}

module.exports = findSumOfAllSalaries;
