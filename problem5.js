//    5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).
const updateSalary = require('./problem3.js');
function getSalariesByCountry(dataset) {
    if(Array.isArray(dataset)) {
        updateSalary(dataset);
        let salariesByCountry = dataset.reduce((accumulator, person) => {
            if(accumulator[person.location]) {
                accumulator[person.location] += person.updated_salary;
            } else {
                accumulator[person.location] = person.updated_salary;
            }
            return accumulator;
        },{})
        return salariesByCountry;
    } else {
        throw new Error('Argument passed to the function getSalariesByCountry is not valid type.')
    }
}
module.exports = getSalariesByCountry;